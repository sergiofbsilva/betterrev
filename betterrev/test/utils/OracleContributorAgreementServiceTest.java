package utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import utils.exception.OCAComunicationException;

/**
 * The unit tests below are using the Given..When..Then style, and hence each
 * step (block of code) is separated by a blank line.
 * 
 */
public class OracleContributorAgreementServiceTest {

	@Test
	public void have_sgined_OCA() throws OCAComunicationException {

		String userFullName = "Ilya Kondratiev";

		boolean canContribute = OracleContributorAgreementService
				.hasSignedOCAforOpenJDK(userFullName);

		assertTrue(
				"User " + userFullName + " has got a signed OCA for OpenJdk",
				canContribute);
	}

	@Test
	public void have_not_sgined_OCA() throws OCAComunicationException {

		String userFullName = "Fred Hacker";

		boolean canContribute = OracleContributorAgreementService
				.hasSignedOCAforOpenJDK(userFullName);

		assertFalse("User " + userFullName
				+ " has not got a signed OCA for OpenJdk", canContribute);
	}

}