package models;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static models.Mentor.MentorType.INDIVIDUAL;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Contribution entity basic persistence tests.
 */
public class ContributionTest extends AbstractPersistenceIntegrationTest {

    private static final String ANOTHER_TEST_REPOSITORY_ID = "better-test-repo";
    private static final String TEST_REPOSITORY_ID = "123";
    private static final String TEST_PULL_REQUEST_ID = "123";
    private static final String TEST_CONTRIBUTION_NAME = "Major Mods!";
    private static final String TEST_CONTRIBUTION_DESCRIPTION = "Some change to the code";
    private static final DateTime TEST_CONTRIBUTION_CREATED_ON = DateTime.now();
    private static final DateTime TEST_CONTRIBUTION_UPDATED_ON = DateTime.now();
    private static final int ZERO = 0;
    private static final String TEST_BRANCH_NAME = "sigh";

    public static Contribution createTestInstance() {
        Contribution contribution = new Contribution(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                                                     TEST_CONTRIBUTION_NAME, TEST_CONTRIBUTION_DESCRIPTION, UserTest.createTestInstance(),
                                                     TEST_CONTRIBUTION_CREATED_ON, TEST_CONTRIBUTION_UPDATED_ON, TEST_BRANCH_NAME);

        contribution.tags.add(TagTest.createTestInstance());
        addContributionGeneratedEvent(contribution);
        addMentor(contribution);
        return contribution;
    }

    public static Contribution createTestInstanceWithRequesterOcaSignedUnknown() {
        Contribution contribution = new Contribution(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                                                     TEST_CONTRIBUTION_NAME, TEST_CONTRIBUTION_DESCRIPTION, UserTest.createTestInstanceWithRequesterOcaSignedUnknown(),
                                                     TEST_CONTRIBUTION_CREATED_ON, TEST_CONTRIBUTION_UPDATED_ON, TEST_BRANCH_NAME);

        contribution.tags.add(TagTest.createTestInstance());
        addContributionGeneratedEvent(contribution);
        addMentor(contribution);
        return contribution;
    }
    
    private static void addContributionGeneratedEvent(Contribution contribution) {
        ContributionEvent event = new ContributionEvent(ContributionEventType.CONTRIBUTION_GENERATED);
        event.contribution = contribution;
        contribution.contributionEvents.add(event);
    }

    private static void addMentor(Contribution contribution) {
        contribution.mentors = new HashSet<>();
        Mentor testMentor = new Mentor("Helpful Openjdk Person", "helpful@example.com", INDIVIDUAL);
        contribution.mentors.add(testMentor);
    }

    public static Contribution createTestInstance(State state) {
        Contribution contribution = ContributionTest.createTestInstance();
        contribution.state = state;
        contribution.save();

        return contribution;
    }

    @Test
    public void validContributionPersistsCorrectly() {
        Contribution contribution = createTestInstance();
        assertThat(contribution.id, is(nullValue()));
        contribution.save();

        assertThat(contribution.id, is(not(nullValue())));
    }

    @Test
    public void validContributionCreatedOnSetCorrectly() {
        Contribution contribution = createTestInstance();
        contribution.save();

        assertThat(contribution.createdOn, is(notNullValue()));
    }

    @Test
    public void validContributionCreatedWithGeneratedEvent() {
        Contribution contribution = createTestInstance();
        contribution.save();

        ContributionEvent firstContributionEvent = contribution.contributionEvents.iterator().next();
        assertThat(firstContributionEvent.contributionEventType, is(ContributionEventType.CONTRIBUTION_GENERATED));
    }

    @Test
    public void shouldReturnOneContributionEventForAGivenType() {
        Contribution contribution = createTestInstance();
        contribution.save();

        Collection<ContributionEvent> contributionEvents = contribution.getContributionEventWithType(ContributionEventType.CONTRIBUTION_GENERATED);
        assertThat(contributionEvents.size(), is(1));

        ContributionEvent firstContributionEvent = contribution.contributionEvents.iterator().next();
        assertThat(firstContributionEvent.contributionEventType, is(ContributionEventType.CONTRIBUTION_GENERATED));
    }

    @Test
    public void validContributionCreatedWithStateNull() {
        Contribution contribution = createTestInstance();
        contribution.save();

        assertThat(contribution.state, is(State.NULL));
    }

    /**
     * The unit test below is using the Given..When..Then style,
     * and hence each step (block of code) is separated by a blank line.
     */
    @Test
    public void shouldReturnAtLeastOneMentorUsingDiffStringFromPullRequestURLForOwner() {
        Contribution contribution = new Contribution(ANOTHER_TEST_REPOSITORY_ID, TEST_REPOSITORY_ID,
                                                     TEST_CONTRIBUTION_NAME, TEST_CONTRIBUTION_DESCRIPTION, UserTest.createTestInstance(),
                                                     TEST_CONTRIBUTION_CREATED_ON, TEST_CONTRIBUTION_UPDATED_ON, TEST_BRANCH_NAME);
        String someDiffString =
                "diff -r 6e9df89ba172268db2ff815caee67cc1c70298b1 -r 218c7ad8deb066b652970cb69bd8256cd7827f44 new_file\n" +
                        "--- /dev/null\n" +
                        "+++ b/new_file\n" +
                        "@@ -0,0 +1,1 @@\n" +
                        "+some new code";

        Set<Mentor> actualMentor = contribution.evaluateMentorsFrom(someDiffString);

        assertThat(actualMentor.size(), is(not(equalTo(ZERO))));
    }
}
